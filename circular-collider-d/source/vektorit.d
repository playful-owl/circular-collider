module vektorit;
import std.math;

alias reaali = float;

struct vektori
{
  reaali x;
  reaali y;
  void nollaa()
  {
    x = 0.0;
    y = 0.0;
  }
  void normalisoi()
  {
    if((abs(x) > 0.0) && (abs(y) > 0.0)){
      reaali len = sqrt(x*x + y*y);
      x = x / len;
      y = y / len;
    }
  }
  this(reaali wx, reaali wy)
  {
    x = wx;
    y = wy;
  }
  vektori opUnary(string op)()
  {
    static if(op=="-") return vektori(-x,-y);
  }
  vektori opBinary(string op)(vektori rhs)
  {
    static if(op=="+") return vektori(x+rhs.x,y+rhs.y);
    else static if(op=="-") return vektori(x-rhs.x,y-rhs.y);
  }
  vektori opBinary(string op)(reaali luku)
  {
    static if(op=="+") return vektori(x+luku,y+luku);
    else static if(op=="-") return vektori(x-luku,y-luku);
    else static if(op=="*") return vektori(x*luku,y*luku);
    else static if(op=="/") return vektori(x/luku,y/luku);
  }
  vektori opBinaryRight(string op)(reaali luku)
  {
    static if(op=="+") return vektori(x+luku,y+luku);
    else static if(op=="-") return vektori(x-luku,y-luku);
    else static if(op=="*") return vektori(x*luku,y*luku);
    else static if(op=="/") return vektori(x/luku,y/luku);
  }
}

reaali magnitudi(const vektori a)
{
  return sqrt(a.x*a.x + a.y*a.y);
}

reaali sqrmagnitudi(const vektori a)
{
  return a.x*a.x + a.y*a.y;
}

reaali pistetulo(const vektori a, const vektori b)
{
  return a.x*b.x + a.y*b.y;
}

reaali ristitulo(const vektori a, const vektori b)
{
  return a.x*b.y - a.y*b.x;
}

vektori skalaariristitulo(const reaali s, const vektori b)
{
  return vektori(-s*b.y,s*b.x);
}

vektori skalaariristitulo(const vektori a, const reaali s)
{
  return vektori(s*a.y,-s*a.x);
}

reaali etäisyys(const vektori a, const vektori b)
{
  return sqrt((a.x - b.x)*(a.x - b.x) +
	      (a.y - b.y)*(a.y - b.y));
}

reaali sqretäisyys(const vektori a, const vektori b)
{
  return ((a.x - b.x)*(a.x - b.x) +
	  (a.y - b.y)*(a.y - b.y));
}

vektori etäisyysvektori(const vektori a, const vektori b)
{
  return vektori(a.x - b.x, a.y - b.y);
}
