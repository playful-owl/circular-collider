import std.file;
import std.json;
import std.conv;
import std.stdio;
import std.getopt;
import imageformats;
//import core.thread;

import gl3n.linalg;
import derelict.opengl;
import derelict.glfw3.glfw3;

import skene;
import vektorit;

immutable int leveys = 800;
immutable int korkeus = 800;

void glfw_vihjeet()
{
  debug(2) writeln("lähetän vihjeitä");
  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
  //glfwWindowHint(GLFW_FLOATING, GLFW_FALSE);
}

void versiot()
{
  writefln("Myyjä:   %s",   to!string(glGetString(GL_VENDOR)));
  writefln("Renderi: %s",   to!string(glGetString(GL_RENDERER)));
  writefln("Versio:  %s",   to!string(glGetString(GL_VERSION)));
  writefln("GLSL:    %s\n", to!string(glGetString(GL_SHADING_LANGUAGE_VERSION)));
}

// tämä ei taida toimia
/*
void capFrameRate(double fps) {
  static double start = 0;
  static double diff;
  static double wait;
  wait = 1.0 / fps;
  diff = glfwGetTime() - start;
  debug(2) { writeln("diff, wait: ",diff," ",wait); }
  if (diff < wait) {
    Thread.sleep( dur!("msecs")( cast(int)wait - cast(int)diff ) );
  }
  start = glfwGetTime();
}
*/

immutable char* kärkivarjostin =
  "#version 410\n" ~
  "layout(location = 0) in vec3 vertpos;" ~
  "layout(location = 1) in vec3 vertcol;" ~
  "out vec3 colour;" ~
  "uniform mat4 mv;" ~
  "void main(){" ~
  "  colour = vertcol;" ~
  "  gl_Position = mv * vec4(vertpos, 1.0);" ~
  "}";

immutable char* sirpalevarjostin = 
  "#version 410\n" ~
  "in vec3 colour;" ~
  "out vec4 outcolour;" ~
  "void main(){" ~
  "  outcolour = vec4(colour, 1.0);" ~
  "}";


void main(string[] args)
{
  string lukutiedostonimi;
  bool talteen = false;
  int pysäytys = 1500;
  getopt(args,
	 "lue",&lukutiedostonimi,
	 "talteen",&talteen,
	 "pysäytys",&pysäytys);
  debug(2) writeln("luen tiedoston: ",lukutiedostonimi);
  if(!lukutiedostonimi.exists()){
    writeln("käyttö: ");
  }else{
    string luku = readText(lukutiedostonimi);
    JSONValue j = parseJSON(luku);
    int lkm = to!int(j["lkm"].integer);
    // argumentit: sijx sijy nopx nopy kulmanopeus massa hitausmomentti
    reaali[] sijx = new reaali[lkm];
    reaali[] sijy = new reaali[lkm];
    reaali[] nopx = new reaali[lkm];
    reaali[] nopy = new reaali[lkm];
    reaali[] nopk = new reaali[lkm];
    reaali[] mass = new reaali[lkm];
    reaali[] hitm = new reaali[lkm];
    // assert pituudet
    for(auto i = 0; i < lkm; ++i){
      sijx[i] = to!reaali(j["sijx"][i].floating);
      sijy[i] = to!reaali(j["sijy"][i].floating);
      nopx[i] = to!reaali(j["nopx"][i].floating);
      nopy[i] = to!reaali(j["nopy"][i].floating);
      nopk[i] = to!reaali(j["nopk"][i].floating);
      mass[i] = to!reaali(j["mass"][i].floating);
      hitm[i] = to!reaali(j["hitm"][i].floating);
    }
    DerelictGL3.load();
    DerelictGLFW3.load();
    if(!glfwInit()){
      glfwTerminate();
      writeln("VIRHE: glfwInit");
    }else{
      GLFWwindow* ikkuna;
      glfw_vihjeet();
      ikkuna = glfwCreateWindow(leveys, korkeus, "IKKUNA", null, null);
      glfwSetWindowSize(ikkuna,leveys,korkeus);
      if(!ikkuna){
	glfwTerminate();
	writeln("VIRHE: glfwCreateWindow");
      }else{
	glfwMakeContextCurrent(ikkuna);
	DerelictGL3.reload();
	versiot();
	glEnable(GL_DEPTH_TEST);
	GLuint kv = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(kv,1,&kärkivarjostin,null);
	glCompileShader(kv);
	GLuint sv = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(sv,1,&sirpalevarjostin,null);
	glCompileShader(sv);
	GLuint varjostin_ohjelma = glCreateProgram();
	glAttachShader(varjostin_ohjelma,sv);
	glAttachShader(varjostin_ohjelma,kv);
	glLinkProgram(varjostin_ohjelma);
	// luodaan skene
	skene s = new skene(lkm);
	// argt: sx, sy, nx, ny, kn, ma, hm
	for(auto i = 0; i < lkm; ++i){
	  s.tee_pallo(sijx[i],sijy[i],
		      nopx[i],nopy[i],nopk[i],
		      mass[i],hitm[i]);
	}
	//s.tee_pallo( 0.5, 0.08,-0.04, 0.0, 0.0, 0.010, 0.000025);
	//s.tee_pallo(-0.5,-0.08, 0.04, 0.0, 1.0, 0.010, 0.000025);
	GLint uniform_mv = glGetUniformLocation(varjostin_ohjelma, "mv");
	ubyte[] puskuri = new ubyte[800*800*3];
	ubyte* p = puskuri.ptr;
	string pohja = "ulos/joo";
	string ekstensio = ".png";
	string num;
	string kokonimi;
	uint askel = 0;
	while(!glfwWindowShouldClose(ikkuna)){
	  //capFrameRate(1); // tämähän ei tee mitään!
	  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	  glUseProgram(varjostin_ohjelma);
	  s.liikuta_kaikki(0.1); // sijainnit -> nan
	  s.tarkista_rajat();
	  s.tarkista_kosketukset();
	  s.piirrä_kaikki(uniform_mv);
	  glfwSwapBuffers(ikkuna);
	  if(talteen){
	    if(true){
	      glReadBuffer(GL_FRONT);
	      glReadPixels(0,0,800,800,GL_RGB,GL_UNSIGNED_BYTE,p);
	      num = to!string(askel);
	      kokonimi = pohja ~ num ~ ekstensio;
	      write_png(kokonimi,cast(long)800,cast(long)800,puskuri);
	    }
	  }
	  glfwPollEvents();
	  if(GLFW_PRESS == glfwGetKey(ikkuna,GLFW_KEY_ESCAPE)){
	    glfwSetWindowShouldClose(ikkuna,1);
	    writeln("ESC painettu");
	    writeln("viimeinen askel: ",askel);
	  }
	  if(askel >= pysäytys){
	    glfwSetWindowShouldClose(ikkuna,1);
	    writeln("saavutettiin pysäytysaskel");
	    writeln("viimeinen askel: ",askel);
	  }
	  askel++;
	}
	glfwDestroyWindow(ikkuna);
	glfwTerminate();
      }
    }
    writeln("valmis!");
  }
}
