module skene;
import std.math;
import std.stdio;
import vektorit;
import fysiikka;
import kosketus;

import derelict.opengl;

class skene
{
public:
  
  uint objlkm;
  kappale[] kappaleet;
  
  this()
  {
    objlkm = 0;
    kappaleet.length = 10;
  }
  
  this(uint wmaxlkm)
  {
    objlkm = 0;
    kappaleet.length = wmaxlkm;
  }
  
  void tee_pallo(reaali wsx, reaali wsy,
		 reaali wnx, reaali wny,
		 reaali wkn,
		 reaali wma, reaali whm)
  {
    kappaleet[objlkm] = new kappale();
    kappaleet[objlkm].alusta();
    kappaleet[objlkm].sijainti = vektori(wsx,wsy);
    kappaleet[objlkm].nopeus = vektori(wnx,wny);
    kappaleet[objlkm].kiihtyvyys = vektori(0.0,0.0);
    kappaleet[objlkm].kulmanopeus = wkn;
    kappaleet[objlkm].massa_oik = wma;
    kappaleet[objlkm].massa_inv = 1.0/wma;
    kappaleet[objlkm].hitausmomentti = whm;
    objlkm = objlkm + 1;
  }
  
  void piirrä_kaikki(GLint unif_mv)
  {
    for(size_t i = 0; i < objlkm; ++i){
      kappaleet[i].piirrä(unif_mv);
    }
  }
  
  void liikuta_kaikki(reaali dt)
  {
    for(size_t i = 0; i < objlkm; ++i){
      kappaleet[i].liikuta(dt);
    }
  }
  
  void kerrytä1(uint ind, reaali kx, reaali ky)
  {
    kappaleet[ind].kerrytä(kx,ky);
  }
  
  bool tarkista_rajat()
  {
    ubyte ylityksiä = 0;
    for(size_t i = 0; i < objlkm; ++i){
      if(kappaleet[i].rxmax > 1.0){
	kappaleet[i].sijainti.x = 1.0 - kappaleet[i].säde;
	kappaleet[i].xraja();
	ylityksiä++;
      }else if(kappaleet[i].rxmin < -1.0){
	kappaleet[i].sijainti.x = -1.0 + kappaleet[i].säde;
	kappaleet[i].xraja();
	ylityksiä++;
      }
      if(kappaleet[i].rymax > 1.0){
	kappaleet[i].sijainti.y = 1.0 - kappaleet[i].säde;
	kappaleet[i].yraja();
	ylityksiä++;
      }else if(kappaleet[i].rymin < -1.0){
	kappaleet[i].sijainti.y = -1.0 + kappaleet[i].säde;
	kappaleet[i].yraja();
	ylityksiä++;
      }
    }
    if(ylityksiä > 0){
      return true;
    }else{
      return false;
    }
  }
  
  bool tarkista_kosketukset()
  {
    ubyte kontakteja = 0;
    if(objlkm > 1){
      for(size_t i = 0; i < objlkm; ++i){
	for(size_t j = i + 1; j < objlkm; ++j){
	  vektori k1sij = kappaleet[i].sijainti;
	  vektori k2sij = kappaleet[j].sijainti;
	  reaali etäisyysx = k1sij.x - k2sij.x;
	  reaali etäisyysy = k1sij.y - k2sij.y;
	  if((etäisyysx < kappaleet[i].säde+kappaleet[j].säde) ||
	     (etäisyysy < kappaleet[i].säde+kappaleet[j].säde)){
	    reaali etäisyys = (etäisyysx*etäisyysx + etäisyysy*etäisyysy);
	    reaali sädesqr = (kappaleet[i].säde + kappaleet[j].säde) *
	      (kappaleet[i].säde + kappaleet[j].säde);
	    if(etäisyys < sädesqr){
	      manifoldi m;
	      hoida(m,&kappaleet[i],&kappaleet[j]);
	      kontakteja++;
	    }
	  }
	}
      }
    }
    if(kontakteja > 0){
      return true;
    }else{
      return false;
    }
  }
}
