module fysiikka;
import std.math;
import std.stdio;

import derelict.opengl;
import gl3n.linalg;

import vektorit;

immutable reaali pii = 3.14159265;

class kappale
{
public:

  bool alustettu = false;
  reaali säde = 0.1;
  reaali massa_oik = 1.0;
  reaali massa_inv = 1.0;
  reaali hitausmomentti = 0.1;
  reaali kulmanopeus = 0.0;
  reaali suunta = 0.0;
  reaali momentti = 0.0;
  vektori sijainti = vektori(0.0,0.0);
  vektori nopeus = vektori(0.0,0.0);
  vektori kiihtyvyys = vektori(0.0,0.0);

  GLuint vao = 0;
  GLuint vbo_pts = 0;
  GLuint vbo_col = 0;
  immutable int samples = 32;

  @property reaali rxmax() { return sijainti.x + säde; }
  @property reaali rxmin() { return sijainti.x - säde; }
  @property reaali rymax() { return sijainti.y + säde; }
  @property reaali rymin() { return sijainti.y - säde; }
  @property reaali kokonaisnopeus() { return abs(nopeus.x) + abs(nopeus.y); }
  
  void aseta(reaali wx, reaali wy)
  {
    sijainti.x = wx;
    sijainti.y = wy;
  }
  
  void aseta_nopeus(reaali wx, reaali wy)
  {
    nopeus.x = wx;
    nopeus.y = wy;
  }
  
  void kerrytä(reaali kx, reaali ky)
  {
    kiihtyvyys.x = kiihtyvyys.x + kx;
    kiihtyvyys.y = kiihtyvyys.y + ky;
  }
  
  void impulsoi(vektori iv, vektori kv)
  {
    nopeus = nopeus + iv*massa_inv;
    kulmanopeus = kulmanopeus + ristitulo(kv,iv)/hitausmomentti; //*iI
  }
  
  void nollaa_voimat()
  {
    kiihtyvyys.nollaa();
  }
  
  void liikuta(reaali dt)
  {
    suunta = suunta + kulmanopeus*dt;
    if(suunta > 2.0*pii) suunta = suunta - 2.0*pii;
    else if(suunta < -2.0*pii) suunta = suunta + 2.0*pii;
    kulmanopeus *= 0.999;
    // symplektinen Euler:
    nopeus = nopeus + (kiihtyvyys * massa_inv)*dt;
    sijainti = sijainti + nopeus*dt;
    nollaa_voimat();
  }

  void hidasta()
  {
    nopeus.x *= 0.95;
    nopeus.y *= 0.95;
  }
  
  void xraja()
  {
    nopeus.x = -1.0*nopeus.x;
    hidasta();
  }
  
  void yraja()
  {
    nopeus.y = -1.0*nopeus.y;
    hidasta();
  }

  void alusta()
  {
    GLfloat[] ymp_p; // ympyrän pisteet
    for(int i = 0; i < 3; ++i) ymp_p ~= 0.0f;
    for(int i = 0; i < (samples + 1); ++i){
      // x = cos a, y = sin a
      ymp_p ~= (cast(GLfloat)(cos(2.0*pii*cast(double)i/cast(double)samples)));
      ymp_p ~= (cast(GLfloat)(sin(2.0*pii*cast(double)i/cast(double)samples)));
      ymp_p ~= 0.0f;
    }
    GLfloat[] ymp_v; // ympyrän väri
    float colour_increment = 1.0f/cast(float)(samples + 2);
    for(int i = 0; i < 3; ++i) ymp_v ~= 0.5f;
    for(int i = 0; i < (samples + 1); ++i){
      // x = cos a, y = sin a  
      ymp_v ~= 1.0f - colour_increment*cast(float)i;
      ymp_v ~= 0.0f + colour_increment*cast(float)i;
      ymp_v ~= 0.0f;
    }
    
    glGenBuffers(1,&vbo_pts);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_pts);
    glBufferData(GL_ARRAY_BUFFER,(2+samples)*3*GLfloat.sizeof,&ymp_p[0],GL_STATIC_DRAW);
    
    glGenBuffers(1,&vbo_col);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_col);
    glBufferData(GL_ARRAY_BUFFER,(2+samples)*3*GLfloat.sizeof,&ymp_v[0],GL_STATIC_DRAW);
  
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER,vbo_pts);
    glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,null);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_col);
    glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,null);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    alustettu = true;
  }
  
  void piirrä(GLint unif_mv)
  {
    if(alustettu){
      mat4 tr, sk, ro, mv;
      tr = mat4.identity();
      tr = tr.translation(cast(float)sijainti.x,
			  cast(float)sijainti.y,
			  0.0f);
      sk = mat4.identity();
      sk = sk.scale(cast(float)säde,
		    cast(float)säde,
		    1.0f);
      ro = mat4.identity();
      ro = ro.rotation(cast(float)suunta,
		       vec3(0.0f,0.0f,1.0f));
      mv = tr * ro * sk;
      glUniformMatrix4fv(unif_mv, 1, GL_TRUE, mv.value_ptr);
      glBindVertexArray(vao);
      glDrawArrays(GL_TRIANGLE_FAN,0,(2+samples));
    }else{
      writeln("VIRHE: kappaletta ei ole alustettu");
    }
  }
}
