module kosketus;
import std.math;
import std.stdio;
import vektorit;
import fysiikka;

struct manifoldi
{
  kappale *k0;
  kappale *k1;
  vektori normaali;
}

// vakiot
immutable double j_kerroin = -2.0;

/*
void hoida(manifoldi m, kappale *wk0, kappale *wk1)
{
  m.k0 = wk0;
  m.k1 = wk1;
  m.normaali = m.k1.sijainti - m.k0.sijainti;
  reaali etäisyys = m.normaali.magnitudi();
  // reaali penetraatio = m.k0.säde + m.k1.säde - etäisyys; // tarvitaanko?
  m.normaali = m.normaali/etäisyys;
  vektori kontakti = m.k0.sijainti + m.k0.säde*m.normaali;
  vektori r0 = kontakti - m.k0.sijainti;
  vektori r1 = kontakti - m.k1.sijainti;
  vektori suht_nopeus = ((m.k1.nopeus + skalaariristitulo(m.k1.kulmanopeus,r1)) -
			 (m.k0.nopeus + skalaariristitulo(m.k0.kulmanopeus,r0)));
  reaali kontaktinopeus = pistetulo(suht_nopeus,m.normaali);
  if(kontaktinopeus > 0.0) return;
  debug(1){
    writeln("ennen: "); logi(m);
  }
  reaali r0_risti_n = ristitulo(r0,m.normaali);
  reaali r1_risti_n = ristitulo(r1,m.normaali);
  reaali inv_massa_summa = (m.k0.massa_inv + m.k1.massa_inv +
			    r0_risti_n*r0_risti_n/m.k0.hitausmomentti +
			    r1_risti_n*r1_risti_n/m.k1.hitausmomentti);
  reaali e = 1.0;
  reaali j = -(1.0 + e)*kontaktinopeus;
  j = j / inv_massa_summa;
  vektori impulssi = m.normaali * j;
  m.k0.impulsoi(-impulssi,r0);
  m.k1.impulsoi(impulssi,r1);
  debug(1){
    writeln("jälkeen: "); logi(m);
  }
}
*/

/*
void hoida(manifoldi m, kappale *wk0, kappale *wk1)
{
  m.k0 = wk0;
  m.k1 = wk1;
  m.normaali = m.k1.sijainti - m.k0.sijainti;
  reaali etäisyys = m.normaali.magnitudi();
  reaali penetraatio = m.k0.säde + m.k1.säde - etäisyys; // tarvitaanko?
  m.normaali = m.normaali/etäisyys;
  vektori suht_nopeus = m.k1.nopeus - m.k0.nopeus;
  reaali kontaktinopeus = pistetulo(suht_nopeus,m.normaali);
  if(kontaktinopeus > 0.0) return;
  debug(1){
    writeln("ennen: "); logi(m);
  }
  vektori kontakti = m.k0.sijainti + m.k0.säde*m.normaali;
  vektori r0 = kontakti - m.k0.sijainti;
  vektori r1 = kontakti - m.k1.sijainti;
  reaali e = 1.0;
  reaali j = -(1.0 + e)*kontaktinopeus;
  j /= (m.k0.massa_inv + m.k1.massa_inv +
	ristitulo(r0,m.normaali)*ristitulo(r0,m.normaali)/m.k0.hitausmomentti +
	ristitulo(r1,m.normaali)*ristitulo(r1,m.normaali)/m.k1.hitausmomentti);
  vektori impulssi = j * m.normaali;
  m.k0.nopeus = m.k0.nopeus - m.k0.massa_inv*impulssi;
  m.k1.nopeus = m.k1.nopeus + m.k1.massa_inv*impulssi;
  m.k0.kulmanopeus = m.k0.kulmanopeus + ristitulo(r0,impulssi)/m.k0.hitausmomentti;
  m.k1.kulmanopeus = m.k1.kulmanopeus - ristitulo(r1,impulssi)/m.k1.hitausmomentti;
  debug(1){
    writeln("jälkeen: "); logi(m);
  }
}
*/


void hoida(manifoldi m, kappale *wk0, kappale *wk1)
{
  m.k0 = wk0;
  m.k1 = wk1;
  m.normaali = m.k1.sijainti - m.k0.sijainti;
  reaali etäisyys = m.normaali.magnitudi();
  reaali penetraatio = m.k0.säde + m.k1.säde - etäisyys; // tarvitaanko?
  m.normaali = m.normaali/etäisyys;
  vektori suht_nopeus = m.k1.nopeus - m.k0.nopeus;
  reaali kontaktinopeus = pistetulo(suht_nopeus,m.normaali);
  if(kontaktinopeus > 0.0) return;
  debug(1){
    writeln("ennen: "); logi(m);
  }
  reaali e = 0.95;
  reaali j = -(1.0 + e)*kontaktinopeus;
  j /= m.k0.massa_inv + m.k1.massa_inv;
  vektori impulssi = j * m.normaali;
  m.k0.nopeus = m.k0.nopeus - m.k0.massa_inv*impulssi;
  m.k1.nopeus = m.k1.nopeus + m.k1.massa_inv*impulssi;
  // lin impulssin päälle friktio
  vektori kontakti = m.k0.sijainti + m.k0.säde*m.normaali;
  vektori r0 = kontakti - m.k0.sijainti;
  vektori r1 = kontakti - m.k1.sijainti;
  vektori rv = ((m.k1.nopeus + skalaariristitulo(m.k1.kulmanopeus,r1)) -
		(m.k0.nopeus + skalaariristitulo(m.k0.kulmanopeus,r0)));
  vektori t = rv - (m.normaali*pistetulo(rv,m.normaali));
  t.normalisoi();
  reaali r0_risti_n = ristitulo(r0,m.normaali);
  reaali r1_risti_n = ristitulo(r1,m.normaali);
  reaali inv_massa_summa = (m.k0.massa_inv + m.k1.massa_inv +
			    r0_risti_n*r0_risti_n/m.k0.hitausmomentti +
			    r1_risti_n*r1_risti_n/m.k1.hitausmomentti);
  reaali jt = -pistetulo(rv,t);
  jt /= inv_massa_summa;
  reaali myy = sqrt(0.5*0.5 + 0.5*0.5);
  reaali df = sqrt(0.3*0.3 + 0.3*0.3);
  vektori tan_impulssi;
  if(abs(jt) < (j*myy)){
    tan_impulssi = t * jt;
  }else{
    tan_impulssi = t* -j * df;
  }
  m.k0.impulsoi(-tan_impulssi,r0);
  m.k1.impulsoi(tan_impulssi,r1);
  debug(1){
    writeln("jälkeen: "); logi(m);
  }
}


void logi(ref manifoldi m)
{
  reaali k0kine = m.k0.kokonaisnopeus*m.k0.kokonaisnopeus*m.k0.massa_oik;
  reaali k1kine = m.k1.kokonaisnopeus*m.k1.kokonaisnopeus*m.k1.massa_oik;
  writeln(" lin: k0: ",k0kine," k1: ",k1kine," summa: ",k0kine+k1kine);
  write(" ang: k0: ");
  writef("%2.15f",m.k0.kulmanopeus);
  write(" k1: ");
  writefln("%2.15f",m.k1.kulmanopeus);
}

/*
void hoida(manifoldi m, kappale *wk0, kappale *wk1)
{
  m.k0 = wk0;
  m.k1 = wk1;
  m.normaali = m.k1.sijainti - m.k0.sijainti;
  reaali etäisyys = m.normaali.magnitudi();
  reaali penetraatio = m.k0.säde + m.k1.säde - etäisyys;
  m.normaali = m.normaali/etäisyys;
  vektori kontakti = m.k0.sijainti + m.k0.säde*m.normaali;
  vektori ra = kontakti - m.k0.sijainti;
  vektori rb = kontakti - m.k1.sijainti;
  vektori suht_nopeus = (m.k1.nopeus + ristitulo(m.k1.kulmanopeus,rb)) -
    (m.k0.nopeus + ristitulo(m.k0.kulmanopeus,ra));
  reaali kontaktinopeus = pistetulo(suht_nopeus,m.normaali);
  if(kontaktinopeus > 0.0) return;
  vektori ka = ristitulo(ra,m.normaali);
  vektori kb = ristitulo(rb,m.normaali);
  vektori ua = m.k0.hitausmomentti*ka;
  vektori ub = m.k1.hitausmomentti*kb;
  reaali numer = -(1.0 + 1.0)*(pistetulo(m.normaali,m.k0.nopeus - m.k1.nopeus) +
			       pistetulo(m.k0.kulmanopus,ka) - pistetulo(m.k1.kulmanopeus,kb));
  reaali denom = m.k0.massa_inv + m.k1.massa_inv + pistetulo(ka,ua) + pistetulo(kb,ub);
  reaali f = numer / denom;
  vektori impulssi = f*m.normaali;
  m.k0.impulsoi(impulssi,f*ka);
  m.k1.impulsoi(-impulssi,-f*kb);
}
*/


/*
void hoitele(manifoldi m, kappale *wk0, kappale *wk1)
{
  m.k0 = wk0;
  m.k1 = wk1;
  m.normaali = m.k1.sijainti - m.k0.sijainti;
  reaali etäisyys = m.normaali.magnitudi();
  reaali penetraatio = m.k0.säde + m.k1.säde - etäisyys;
  m.normaali = m.normaali/etäisyys;
  vektori kontakti = m.k0.sijainti + m.k0.säde*m.normaali;
  vektori ra = kontakti - m.k0.sijainti;
  vektori rb = kontakti - m.k1.sijainti;
  vektori suht_nopeus = (m.k1.nopeus + ristitulo(m.k1.kulmanopeus,rb)) -
    (m.k0.nopeus + ristitulo(m.k0.kulmanopeus,ra));
  reaali kontaktinopeus = pistetulo(suht_nopeus,m.normaali);
  if(kontaktinopeus > 0.0) return;
  debug(1){
    writeln("ennen:");
    writeln(" k0: ",m.k0.kokonaisnopeus*m.k0.massa_oik,
	    " k1: ",m.k1.kokonaisnopeus*m.k1.massa_oik);
  }
  reaali ra_risti_n = ristitulo(ra,m.normaali);
  reaali rb_risti_n = ristitulo(rb,m.normaali);
  reaali inv_massa_summa = m.k0.massa_inv + m.k1.massa_inv +
    ra_risti_n*ra_risti_n*m.k0.hitausmomentti +
    rb_risti_n*rb_risti_n*m.k1.hitausmomentti;
  reaali j = j_kerroin * kontaktinopeus / inv_massa_summa;
  vektori impulssi = j*m.normaali;
  m.k0.impulsoi(-impulssi,ra);
  m.k1.impulsoi(impulssi,rb);
  debug(1){
    writeln("jälkeen:");
    writeln(" k0: ",m.k0.kokonaisnopeus*m.k0.massa_oik,
	    " k1: ",m.k1.kokonaisnopeus*m.k1.massa_oik);
  }
}

void hoida_friktiolla(manifoldi m, kappale *wk0, kappale *wk1)
{
  m.k0 = wk0; // nämä voisi asettaa kun manifoldi tehdään?
  m.k1 = wk1;
  m.normaali = m.k1.sijainti - m.k0.sijainti;
  reaali etäisyys = m.normaali.magnitudi(); // etäisyys 0.0 kaataa...
  reaali penetraatio = m.k0.säde + m.k1.säde - etäisyys;
  m.normaali = m.normaali/etäisyys;
  vektori kontakti = m.k0.sijainti + m.k0.säde*m.normaali;
  vektori ra = kontakti - m.k0.sijainti;
  vektori rb = kontakti - m.k1.sijainti;
  vektori rv = m.k1.nopeus + ristitulo(m.k1.kulmanopeus,rb)
    - m.k0.nopeus - ristitulo(m.k0.kulmanopeus,ra);
  reaali kontaktinopeus = pistetulo(rv,m.normaali);
  if(kontaktinopeus > 0.0) return;
  writeln("alussa: k0 kulmanopeus ",m.k0.kulmanopeus,
	  " k1 kulmanopeus ",m.k1.kulmanopeus);
  reaali ra_risti_n = ristitulo(ra,m.normaali);
  reaali rb_risti_n = ristitulo(rb,m.normaali);
  reaali inv_massa_summa = m.k0.massa_inv + m.k1.massa_inv + 
    ra_risti_n*ra_risti_n * m.k0.hitausmomentti +
    rb_risti_n*rb_risti_n * m.k1.hitausmomentti; // onhan oikein päin?
  reaali j = j_kerroin * kontaktinopeus;
  j = j / inv_massa_summa;
  vektori impulssi = j * m.normaali;
  writeln("kontakti: etäisyys ",etäisyys," penetraatio ",penetraatio,
	  " kontakti-x ",kontakti.x," kontakti-y ",kontakti.y,
	  " ra-x ",ra.x," ra-y ",ra.y," rb-x ",rb.x," rb-y ",rb.y,
	  " kontaktinopeus ",kontaktinopeus," j ",j);
  writeln("imp-x ",impulssi.x," imp-y ",impulssi.y," risti-1 ",ristitulo(-impulssi,ra),
	  " risti-2 ",ristitulo(impulssi,rb));
  m.k0.impulsoi(-impulssi,ra);
  m.k1.impulsoi(impulssi,rb);
  // kitkaa:
  rv = m.k1.nopeus + ristitulo(m.k1.kulmanopeus,rb) 
    - m.k0.nopeus - ristitulo(m.k0.kulmanopeus,ra);
  vektori t = rv - (m.normaali * pistetulo(rv,m.normaali));
  if((t.x < 0.0001) && (t.y < 0.0001)){
    writeln("t < 0.0001");
    return;
  }
  t.normalisoi();
  reaali jt = -1.0 * pistetulo(rv,t);
  jt = jt / inv_massa_summa; // pelkkä inv_massa_summa ?
  write("tangentiaali ",jt," t-x ",t.x," t-y ",t.y);
  reaali myy = sqrt(0.5*0.5 + 0.5*0.5);
  vektori tan_impulssi;
  if(abs(jt) < j*myy){
    tan_impulssi = t * jt;
    writeln(" abs(jt) < j*myy -> ali");
  }else{
    tan_impulssi = t * j * sqrt(0.3*0.3 + 0.3*0.3);
    writeln(" abs(jt) < j*myy -> yli");
  }
  m.k0.impulsoi(-tan_impulssi,ra);
  m.k1.impulsoi(tan_impulssi,rb);
  writeln("lopussa: k0 kulmanopeus ",m.k0.kulmanopeus,
	  " k1 kulmanopeus ",m.k1.kulmanopeus);
}
*/
