#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "scene.h"

const int gl_width = 640;     //!< ikkunan leveys
const int gl_height = 480;    //!< ikkunan korkeus
GLFWwindow *window = nullptr; //!< window pointer

#include "glutils.h"

/** for reading the glsl files */
bool parse_file_into_str(const char *filename, char *shader_str, int max_len);

const char *GL_type_to_string(GLenum type);
void print_shader_info_log(GLuint shader_index);
void print_programme_info_log(GLuint sp);
void print_all(GLuint sp);
bool is_valid(GLuint sp);

int main()
{
  GLuint vs, fs, programma;
  const GLchar *p;
  int params = -1;
  startgl();
  char vertex_shader[1024 * 256];
  char fragment_shader[1024 * 256];
  parse_file_into_str("vs_circle.glsl",vertex_shader,1024 * 256);
  parse_file_into_str("fs_circle.glsl",fragment_shader,1024 * 256);
  vs = glCreateShader(GL_VERTEX_SHADER);
  p = (const GLchar *)vertex_shader;
  glShaderSource(vs,1,&p,NULL);
  glCompileShader(vs);
  /* check for shader compile errors - very important! */
  glGetShaderiv(vs,GL_COMPILE_STATUS,&params);
  if(GL_TRUE != params){
    fprintf(stderr,"ERROR: GL shader index %i did not compile\n",vs);
    print_shader_info_log(vs);
    return 1;
  }
  fs = glCreateShader(GL_FRAGMENT_SHADER);
  p = (const GLchar *)fragment_shader;
  glShaderSource(fs,1,&p,NULL);
  glCompileShader(fs);
  /* check for compile errors */
  glGetShaderiv(fs,GL_COMPILE_STATUS,&params);
  if(GL_TRUE != params){
    fprintf(stderr,"ERROR: GL shader index %i did not compile\n",fs);
    print_shader_info_log(fs);
    return 1;
  }

  programma = glCreateProgram();
  glAttachShader(programma,fs);
  glAttachShader(programma,vs);
  glLinkProgram(programma);
  
  /* check for shader linking errors - very important! */
  glGetProgramiv(programma,GL_LINK_STATUS,&params);
  if(GL_TRUE != params){
    fprintf(stderr,"ERROR: could not link shader programme GL index %i\n",
             programma);
    print_programme_info_log(programma);
    return 1;
  }
  print_all(programma);
  bool result = is_valid(programma);
  assert(result);
  
  //circle_program_id = loadshaders("cshader.vert", "cshader.frag");
  //GLuint vertexposition_modelspace_id = glGetAttribLocation(circle_program_id,
  //							    "vertexPosition_modelspace");
  scene skene(600.0f,400.0f);
  skene.place_circle(2.0f,1.0f,300.0f,300.0f,1);
  //vertexposition_modelspace_id);
  skene.cleanup();
  glfwTerminate();
  std::cout << "toimi" << std::endl;
  return 0;
}

void print_shader_info_log(GLuint shader_index){
  int max_length = 2048;
  int actual_length = 0;
  char log[2048];
  glGetShaderInfoLog(shader_index,max_length,&actual_length,log);
  printf("shader info log for GL index %i:\n%s\n",shader_index,log);
}

void print_programme_info_log(GLuint sp){
  int max_length = 2048;
  int actual_length = 0;
  char log[2048];
  glGetProgramInfoLog(sp,max_length,&actual_length,log);
  printf("program info log for GL index %i:\n%s",sp,log);
}

bool is_valid(GLuint sp){
  int params = -1;
  glValidateProgram(sp);
  glGetProgramiv(sp,GL_VALIDATE_STATUS,&params);
  printf("program %i GL_VALIDATE_STATUS = %i\n",sp,params);
  if(GL_TRUE != params){
    print_programme_info_log(sp);
    return false;
  }
  return true;
}

void print_all(GLuint sp){
  int params = -1;
  printf("--------------------\nshader programme %i info:\n",sp);
  glGetProgramiv(sp,GL_LINK_STATUS,&params);
  printf("GL_LINK_STATUS = %i\n",params);
  glGetProgramiv(sp,GL_ATTACHED_SHADERS,&params);
  printf("GL_ATTACHED_SHADERS = %i\n",params);
  glGetProgramiv(sp,GL_ACTIVE_ATTRIBUTES,&params);
  printf("GL_ACTIVE_ATTRIBUTES = %i\n",params);
  for(int i = 0; i < params; i++){
    char name[64];
    int max_length = 64;
    int actual_length = 0;
    int size = 0;
    GLenum type;
    glGetActiveAttrib(sp,i,max_length,&actual_length,
                       &size,&type,name);
    if(size > 1){
      for(int j = 0; j < size; j++){
        char long_name[64];
        int location;
        sprintf(long_name,"%s[%i]",name,j);
        location = glGetAttribLocation(sp,long_name);
        printf("  %i) type:%s name:%s location:%i\n",
                i,GL_type_to_string(type),long_name,location);
      }
    } else {
      int location = glGetAttribLocation(sp,name);
      printf("  %i) type:%s name:%s location:%i\n",
              i,GL_type_to_string(type),
              name,location);
    }
  }
  glGetProgramiv(sp,GL_ACTIVE_UNIFORMS,&params);
  printf("GL_ACTIVE_UNIFORMS = %i\n",params);
  for(int i = 0; i < params; i++){
    char name[64];
    int max_length = 64;
    int actual_length = 0;
    int size = 0;
    GLenum type;
    glGetActiveUniform(sp,i,max_length,&actual_length,&size,&type,name);
    if(size > 1){
      for(int j = 0; j < size; j++){
        char long_name[64];
        int location;
        sprintf(long_name,"%s[%i]",name,j);
        location = glGetUniformLocation(sp,long_name);
        printf("  %i) type:%s name:%s location:%i\n",
                i,GL_type_to_string(type),
                long_name,location);
      }
    } else {
      int location = glGetUniformLocation(sp,name);
      printf("  %i) type:%s name:%s location:%i\n",
              i,GL_type_to_string(type),
              name,location);
    }
  }
  print_programme_info_log(sp);
}


bool parse_file_into_str(const char *filename,
                          char *shader_str,
                          int max_len){
  FILE *file = fopen(filename,"r");
  if(!file){
    gl_log_err("ERROR: opening file for reading: %s\n",filename);
    return false;
  }
  size_t cnt = fread(shader_str,1,max_len - 1,file);
  if((int)cnt >= max_len - 1){
    gl_log_err("WARNING: file %s too big - truncated.\n",filename);
  }
  if(ferror(file)) {
    gl_log_err("ERROR: reading shader file %s\n",filename);
    fclose(file);
    return false;
  }
  // append \0 to end of file string
  shader_str[cnt] = 0;
  fclose(file);
  return true;
}

const char *GL_type_to_string(GLenum type){
  switch(type){
  case GL_BOOL:
    return "bool";
  case GL_INT:
    return "int";
  case GL_FLOAT:
    return "float";
  case GL_FLOAT_VEC2:
    return "vec2";
  case GL_FLOAT_VEC3:
    return "vec3";
  case GL_FLOAT_VEC4:
    return "vec4";
  case GL_FLOAT_MAT2:
    return "mat2";
  case GL_FLOAT_MAT3:
    return "mat3";
  case GL_FLOAT_MAT4:
    return "mat4";
  case GL_SAMPLER_2D:
    return "sampler2D";
  case GL_SAMPLER_3D:
    return "sampler3D";
  case GL_SAMPLER_CUBE:
    return "samplerCube";
  case GL_SAMPLER_2D_SHADOW:
    return "sampler2DShadow";
  default:
    break;
  }
  return "other";
}
