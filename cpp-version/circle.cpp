#include "circle.h"

void circle::flipx()
{
  vel.x = -1.0f*vel.x;
}

void circle::flipy()
{
  vel.y = -1.0f*vel.y;
}

void circle::clear_acc()
{
  acc.zero();
}

void circle::move(const float dt)
{
  dir = dir + avel*dt;
  if(dir > 2.0*pii){
    dir = dir - 2.0*pii;
  }else if(dir < -2.0*pii){
    dir = dir + 2.0*pii;
  }
  vel = vel + (acc * imass)*dt;
  loc = loc + vel*dt;
  clear_acc();
}

void circle::init(GLuint vertex_position_id)
{
  std::vector<GLfloat> vertices;
  std::vector<GLuint> indices;
  int indicator = 0;
  for(int i = 0; i <= lats; i++){
    double lat0 = pii * (-0.5 + (double) (i - 1) / lats);
    double z0  = sin(lat0);
    double zr0 =  cos(lat0);
    double lat1 = pii * (-0.5 + (double) i / lats);
    double z1 = sin(lat1);
    double zr1 = cos(lat1);
    for(int j = 0; j <= longs; j++){
      double lng = 2 * pii * (double) (j - 1) / longs;
      double x = cos(lng);
      double y = sin(lng);
      vertices.push_back(x * zr0);
      vertices.push_back(y * zr0);
      vertices.push_back(z0);
      indices.push_back(indicator);
      indicator++;
      vertices.push_back(x * zr1);
      vertices.push_back(y * zr1);
      vertices.push_back(z1);
      indices.push_back(indicator);
      indicator++;
    }
    indices.push_back(GL_PRIMITIVE_RESTART_FIXED_INDEX);
  }
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  glGenBuffers(1, &vbovertex);
  glBindBuffer(GL_ARRAY_BUFFER, vbovertex);
  glBufferData(GL_ARRAY_BUFFER,
	       vertices.size() * sizeof(GLfloat),
	       &vertices[0],
	       GL_STATIC_DRAW);
  glVertexAttribPointer(vertex_position_id, 3, GL_FLOAT, GL_FALSE, 0, NULL);
  glEnableVertexAttribArray (vertex_position_id);
  glGenBuffers(1, &vboindex);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
	       vboindex);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,
	       indices.size() * sizeof(GLuint),
	       &indices[0],
	       GL_STATIC_DRAW);
  numsToDraw = indices.size();
  initiated = true;
}

void circle::cleanup()
{
  if(!initiated){
    return;
  }
  if(vbovertex){
    glDeleteBuffers(1, &vbovertex);
  }
  if(vboindex){
    glDeleteBuffers(1, &vboindex);
  }
  if(vao){
    glDeleteVertexArrays(1, &vao);
  }
  initiated = false;
  vao = 0;
  vbovertex = 0;
  vboindex = 0;
}

void circle::draw()
{
  if(!initiated){
    std::cout << "please call init() before draw()" << std::endl;
  }
  // draw sphere
  glBindVertexArray(vao);
  glEnable(GL_PRIMITIVE_RESTART);
  glPrimitiveRestartIndex(GL_PRIMITIVE_RESTART_FIXED_INDEX);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboindex);
  glDrawElements(GL_QUAD_STRIP, numsToDraw, GL_UNSIGNED_INT, NULL);
}
