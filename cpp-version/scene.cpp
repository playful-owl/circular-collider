#include "scene.h"

void scene::place_circle(float rr, float mm, float xx, float yy,
			 GLuint vpos_modspace_id)
{
  circles.push_back(circle(rr,mm,xx,yy));
  circles[objcount].init(vpos_modspace_id);
  objcount++;
}

void scene::draw()
{
  for(int i = 0; i < objcount; ++i){
    circles[i].draw();
  }
}

void scene::cleanup()
{
  for(int i = 0; i < objcount; ++i){
    circles[i].cleanup();
  }
}
