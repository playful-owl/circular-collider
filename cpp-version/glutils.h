#ifndef GLUTILS_H_
#define GLUTILS_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdarg.h>

#define GL_LOG_FILE "gl.log"

extern int gl_width;
extern int gl_height;
extern GLFWwindow *window;

bool startgl();

bool gl_log_err( const char *message, ... );

#endif
