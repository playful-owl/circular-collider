#include "glutils.h"

#include <iostream>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#define GL_LOG_FILE "gl.log"
#define MAX_SHADER_LENGTH 262144

bool startgl() {
  const GLubyte *renderer;
  const GLubyte *version;
  //gl_log( "starting GLFW %s", glfwGetVersionString() );
  //glfwSetErrorCallback( glfw_error_callback );
  if(!glfwInit()){
    std::cout << "ERROR: could not start GLFW3" << std::endl;
    return false;
  }
  glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
  glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 2 );
  glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE );
  glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
  /*GLFWmonitor* mon = glfwGetPrimaryMonitor ();
    const GLFWvidmode* vmode = glfwGetVideoMode (mon);
    g_window = glfwCreateWindow (
    vmode->width, vmode->height, "Extended GL Init", mon, nullptr
    );*/
  window = glfwCreateWindow(gl_width,
			    gl_height,
			    "WINDOW",
			    nullptr,
			    nullptr);
  if(!window){
    std::cout << "ERROR: could not open window with GLFW3" << std::endl;
    glfwTerminate();
    return false;
  }
  //glfwSetWindowSizeCallback( window, glfw_window_size_callback );
  glfwMakeContextCurrent( window );
  glfwWindowHint( GLFW_SAMPLES, 4 );
  /* start GLEW extension handler */
  glewExperimental = GL_TRUE;
  glewInit();
  /* get version info */
  renderer = glGetString( GL_RENDERER ); /* get renderer string */
  version = glGetString( GL_VERSION );   /* version as a string */
  std::cout << "Renderer: " << renderer << std::endl;
  std::cout << "OpenGL version supported: " << version << std::endl;
  //gl_log( "renderer: %s\nversion: %s\n", renderer, version );
  //previous_seconds = glfwGetTime();
  return true;
}

bool gl_log_err( const char *message, ... ) {
  va_list argptr;
  FILE *file = fopen( GL_LOG_FILE, "a" );
  if ( !file ) {
    fprintf( stderr, "VIRHE: GL_LOG_FILE %s ei jatku\n",GL_LOG_FILE );
    return false;
  }
  va_start( argptr, message );
  vfprintf( file, message, argptr );
  va_end( argptr );
  va_start( argptr, message );
  vfprintf( stderr, message, argptr );
  va_end( argptr );
  fclose( file );
  return true;
}
