#ifndef CIRCLE_H_
#define CIRCLE_H_

#include <GL/glew.h>
#include "maths.h"
#include <array>
#include <vector>
#include <iostream>

const std::array<float,16> xcirc{ 0.0f, 0.383f, 0.707f, 0.924f,
    1.0f, 0.924f, 0.707f, 0.383f, 0.0f, -0.383f, -0.707f,
    -0.924f, -1.0f, -0.924f, -0.707f, -0.383f };

const std::array<float,16> ycirc{ 1.0f, 0.924f, 0.707f, 0.383f,
    0.0f, -0.383f, -0.707f, -0.924f, -1.0f, -0.924f, -0.707f,
    -0.383f, 0.0f, 0.383f, 0.707f, 0.924f };

class circle
{
 private:
  GLuint vao;
  GLuint vbovertex;
  GLuint vboindex;
  bool initiated;
  int numsToDraw;
  int lats = 40;
  int longs = 40;
  
 public:
  float rad;
  float mass;
  float imass;
  float avel;
  float dir;
  float torq;
  vec loc;
  vec vel;
  vec acc;
  
 circle(float r) :
  rad(r), mass(1.0f), imass(1.0f), avel(0.0f),
    dir(0.0f), torq(0.0f), loc(0.0f,0.0f),
    vel(0.0f,0.0f), acc(0.0f,0.0f) {}

 circle(float r, float m, float x, float y) :
  rad(r), mass(m), imass(0.0f), avel(0.0f),
    dir(0.0f), torq(0.0f), loc(x,y),
    vel(0.0f,0.0f), acc(0.0f,0.0f)
    {
      imass = 1.0f/m;
    }

  void init(GLuint vertex_position_id);
  void cleanup();
  void draw();
  
  //void stop();
  void flipx();
  void flipy();
  void clear_acc();
  void move(const float dt);

  float rxmax() { return loc.x + rad; }
  float rxmin() { return loc.x - rad; }
  float rymax() { return loc.y + rad; }
  float rymin() { return loc.y - rad; }
};

#endif
