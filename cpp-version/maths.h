#ifndef MATHS_H_
#define MATHS_S_

#include <cmath>

const float pii = 3.14159265;

class vec
{
 public:
  float x;
  float y;

 vec() :
  x(0.0), y(0.0) {}
  
 vec(float xx, float yy) :
  x(xx), y(yy) {}

  //void normalise();
  //float magnitude() const;
  //float sqrtmagnitude() const;
  //float dotprod(vec &other) const;

  void zero()
  {
    x = 0.0;
    y = 0.0;
  }

  vec operator*(const float value)
  {
    return vec(x*value, y*value);
  }

  vec operator+(const vec& v)
  {
    return vec(x + v.x, y + v.y);
  }
  
  void operator-=(const vec& v)
  {
    x -= v.x;
    y -= v.y;
  }
  
  float sqrmagnitude() const
  {
    return x*x+y*y;
  }
};

#endif
