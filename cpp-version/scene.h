#ifndef SCENE_H_
#define SCENE_H_

#include <vector>

#include "circle.h"

class scene
{
 private:
  int objcount;
  std::vector<circle> circles;
  float xmax;
  float ymax;

 public:
 scene(float xx, float yy) :
  objcount(0), xmax(xx), ymax(yy) {}

  void place_circle(float rr, float mm, float xx, float yy,
		    GLuint vpos_modspace_id);
  void draw();
  void cleanup();
  
};
  
#endif
