#ifndef SPHERE_H_
#define SPHERE_H_

#include "maths.h"
#include <array>

const std::array<float,16> xcirc{ 0.0f, 0.383f, 0.707f, 0.924f,
    1.0f, 0.924f, 0.707f, 0.383f, 0.0f, -0.383f, -0.707f,
    -0.924f, -1.0f, -0.924f, -0.707f, -0.383f };

const std::array<float,16> ycirc{ 1.0f, 0.924f, 0.707f, 0.383f,
    0.0f, -0.383f, -0.707f, -0.924f, -1.0f, -0.924f, -0.707f,
    -0.383f, 0.0f, 0.383f, 0.707f, 0.924f };

class sphere
{
 private:
  float m_rad;
  float m_mass;
  float m_imass;
  float m_avel;
  float m_dir;
  float m_torq;
  vec m_loc;
  vec m_vel;
  vec m_acc;

 public:

 sphere(float r) :
  m_rad(r), m_mass(1.0f), m_imass(1.0f), m_avel(0.0f),
    m_dir(0.0f), m_torq(0.0f), m_loc(0.0f,0.0f),
    m_vel(0.0f,0.0f), m_acc(0.0f,0.0f) {}

 sphere(float r, float m, float x, float y) :
  m_rad(r), m_mass(m), m_imass(0.0f), m_avel(0.0f),
    m_dir(0.0f), m_torq(0.0f), m_loc(x,y),
    m_vel(0.0f,0.0f), m_acc(0.0f,0.0f) {
    m_imass = 1.0f/m;
  }
  
  float rad() const { return m_rad; }
  //void stop();
  //void draw();
};

#endif
